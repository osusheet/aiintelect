import argparse
import json
import numpy as np

from methods import pearson_score, find_similar_users

def build_arg_parser():
    parser = argparse.ArgumentParser(description='Find users who are similar to the input user ')
    parser.add_argument('--user', dest='user', required=True,
            help='Input user')
    return parser

if __name__=='__main__':
    args = build_arg_parser().parse_args()
    user = args.user
    ratings_file = 'ratings.json'
    with open(ratings_file, 'r') as f:
        data = json.loads(f.read())
    print('\nUsers similar to ' + user + ':\n')
    similar_users = find_similar_users(data, user, 3)
    print('User\t\t\tSimilarity score')
    print('-' * 41)
    for item in similar_users:
        print(item[0], '\t\t', round(float(item[1]), 2))